"""init

Revision ID: 5fd6f91ab417
Revises: 
Create Date: 2023-06-12 12:51:36.180585

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "5fd6f91ab417"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "user",
        sa.Column("id", sa.Integer, nullable=False, autoincrement=True),
        sa.Column("username", sa.String, nullable=False),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("username"),
    )
    op.create_table(
        "action",
        sa.Column("id", sa.Integer, nullable=False, autoincrement=True),
        sa.Column("user_id", sa.Integer, nullable=False),
        sa.PrimaryKeyConstraint("id"),
        sa.ForeignKeyConstraint(["user_id"], ["user.id"]),
    )


def downgrade() -> None:
    op.drop_table("action")
    op.drop_table("user")
