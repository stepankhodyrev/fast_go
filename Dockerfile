FROM            python:3.11-slim

ENV             LANG C.UTF-8
ENV             PROJECTPATH=/home/app/project
ENV             USER app

RUN             set -x && apt -qq update \
                && apt install -y --no-install-recommends \
                gcc libpq-dev curl git \
                && apt purge -y --auto-remove\
                && rm -rf /var/lib/apt/lists/*

RUN             useradd -m -d /home/${USER} ${USER} \
                && chown -R ${USER} /home/${USER}

RUN             mkdir -p ${PROJECTPATH}

ADD             https://github.com/ufoscout/docker-compose-wait/releases/download/2.9.0/wait ${PROJECTPATH}/wait
RUN             chmod +x ${PROJECTPATH}/wait

RUN             curl -sSL https://install.python-poetry.org | POETRY_HOME=/opt/poetry python3.11 && \
                ln -s /opt/poetry/bin/poetry  /usr/local/bin && \
                poetry config virtualenvs.create false

WORKDIR         ${PROJECTPATH}

ADD             poetry.lock pyproject.toml alembic.ini ${PROJECTPATH}
RUN             poetry install --no-root

COPY            ./src ${PROJECTPATH}
COPY            ./migrations ${PROJECTPATH}

USER            ${USER}