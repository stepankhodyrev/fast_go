from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import relationship

from db import Base


class User(Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True, index=True)
    username = Column(String)
    actions = relationship("Action", backref="user")
