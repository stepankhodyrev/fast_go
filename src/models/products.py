import enum
from sqlalchemy import Column, String, Integer, Enum

from db import Base


class ProductEnums(str, enum.Enum):
    MONITOR = "monitor"
    PRINTER = "printer"
    SCANNER = "scanner"
    LAPTOP = "laptop"
    TABLET = "tablet"


class Product(Base):
    __tablename__ = "product"
    id = Column(Integer, primary_key=True, index=True)
    type = Column(String, Enum(ProductEnums), default=ProductEnums.LAPTOP)
