import uuid
from datetime import datetime
import enum
from sqlalchemy import Column, String, Integer, DateTime, ForeignKey, Enum

from db import Base


class ActionType(str, enum.Enum):
    PURCHASE = "purchase"
    VISIT = "visit"


class Action(Base):
    __tablename__ = "action"
    id = Column(String, primary_key=True, index=True, default=uuid.uuid4())
    datetime = Column(DateTime, default=datetime.now())
    action_type = Column(String, Enum(ActionType), default=ActionType.VISIT)
    user_id = Column(Integer, ForeignKey("user.id"))
    website_id = Column(Integer, nullable=False)
    page_id = Column(Integer, nullable=True)
