from typing import Optional, Union, Set
from datetime import datetime

from pydantic import BaseModel

from models.actions import ActionType
from .users import UserSchema


class ActionSchema(BaseModel):
    user: UserSchema
    website: Union[list, int]
    time: datetime
    action_type: ActionType
    page_id: Optional[int]

    class Config:
        orm_mode = True
