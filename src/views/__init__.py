from fastapi import APIRouter

from views.action_views import action_router

base_router = APIRouter()

base_router.include_router(action_router)
