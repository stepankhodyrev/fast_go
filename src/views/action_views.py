from fastapi import APIRouter
from sqlalchemy import select

import models
import schemas
from db import get_session

session = get_session()
action_router = APIRouter(
    prefix="/action",
    tags=["actions"],
    responses={404: {"code": 404, "description": "Not found action."}},
)


@action_router.get("", response_model=list[schemas.ActionSchema])
async def get_action_list() -> list[models.Action]:
    res = await session.execute(select(models.Action))
    return res.scalars().all()
