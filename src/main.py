from fastapi import FastAPI

from db import engine, get_session
from db import Base

from views import base_router


app = FastAPI()
session = get_session()


@app.on_event("startup")
async def shutdown():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await session.close()
    await engine.dispose()


app.include_router(base_router)
