#!/bin/bash

set -e
# Create base migrations


if [[ $DEBUG -eq '0' ]]; then
  # production mode
  alembic upgrade head
  uvicorn main:app --host "0.0.0.0" --port "5000"
else
  alembic upgrade head
  # development mode
  uvicorn main:app --host "0.0.0.0" --port "5000" --reload
fi